//
//  AdherenceViewController.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/20/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import UIKit

protocol AdherenceViewControllerProtocol: AnyObject {
    func handle(error: DisplayError)
}

class AdherenceViewController: UIViewController {
    var previousAction: (()->())? {
        didSet {
            headerView.previousButtonAction = { [weak self] in
                self?.previousAction?()
            }
        }
    }

    var nextAction: (()->())? {
        didSet {
            headerView.nextButtonAction = { [weak self] in
                self?.nextAction?()
            }
        }
    }
    
    var presenter: AdherencePresenterProtocol?
        
    private var headerView = AdherenceHeaderView {
        $0.dateText = "Today, Dec 12"
        $0.descriptionText = "Good afternoon, Qa"
    }
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.showsVerticalScrollIndicator = false
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(RemedyTableViewCell.self, forCellReuseIdentifier: String(describing: RemedyTableViewCell.self))
        return tableView
    }()
    
    init(previousAction: (()->())?, nextAction: (()->())?) {
        self.previousAction = previousAction
        self.nextAction = nextAction
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setupView()
    }
    
    private func setupView() {
        tableView.backgroundColor = .clear
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: tableView, attribute: .top, relatedBy: .equal, toItem: view.safeAreaLayoutGuide, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: tableView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 15),
            NSLayoutConstraint(item: tableView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: -15),
            NSLayoutConstraint(item: tableView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: headerView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: view.frame.width - 30)
        ])
    }
}

extension AdherenceViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: RemedyTableViewCell.self),
                                                       for: indexPath) as? RemedyTableViewCell else {
            return RemedyTableViewCell()
        }
        cell.update()
        return cell
    }
}

extension AdherenceViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let remedyView = RemedyView(frame: view.frame, dateText: "dsgfsdf", descriptionText: "sdfsdfs", statusText: "dsfsdf")
        let keyWindow = UIApplication.shared.connectedScenes
        .filter({$0.activationState == .foregroundActive})
        .map({$0 as? UIWindowScene})
        .compactMap({$0})
        .first?.windows
        .filter({$0.isKeyWindow}).first
        keyWindow?.addSubview(remedyView)
    }
}

extension AdherenceViewController: AdherenceViewControllerProtocol {}
