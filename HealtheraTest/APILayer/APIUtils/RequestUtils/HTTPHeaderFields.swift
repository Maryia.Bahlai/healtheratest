//
//  HTTPHeaderFields.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/19/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

typealias HTTPHeaders = [String: String]

struct HTTPHeaderFields {
    static let contentType = "Content-Type"
    static let accept = "Accept"
    static let token = "Token"
    static let clientId = "client-id"
    static let appPlatform = "app-platform"
    static let appVersion = "app-version"
}
