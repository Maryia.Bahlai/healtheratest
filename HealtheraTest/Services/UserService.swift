//
//  UserService.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/19/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

protocol UserServiceProtocol {
    var shouldAuthorize: Bool { get }
    func login(username: String, password: String, _ complition: @escaping ExecuteCompletion<Void?>)
    func getAllAdherences(completion: @escaping ExecuteCompletion<ExecuteAdherencesModel>)
    func getRemedy(with id: String, completion: @escaping ExecuteCompletion<ExecuteRemedyModel>)
    func logout(completion: @escaping ExecuteCompletion<Void?>)
}

class UserService: UserServiceProtocol {
    private enum Constant {
        static let mockClientId = "a4c7fdd994b14c0758e91dc997426f043868d4305702f4484220df51d56497b3"
        static let mockAppVersion = "1.4"
    }
    
    var shouldAuthorize: Bool { return user == nil }
    
    @CodableUserDefault(key: "user", defaultValue: nil)
    
    private var user: AppUser?
    private var apiEngine: APIEngineProtocol
    
    init(_ apiEngine: APIEngineProtocol) {
        self.apiEngine = apiEngine
    }
    
    func login(username: String, password: String, _ complition: @escaping ExecuteCompletion<Void?>) {
        var authModel = AuthModel(username: username,
                                  password: password,
                                  clientId: Constant.mockClientId,
                                  deviceToken: DeviceInfo.getDeviceToken())
        if let clientId = user?.clientId {
            authModel.clientId = clientId
        }
        let endPoint = HealtheraEndPoint.login(authModel)
        apiEngine.execute(for: endPoint, model: LoginModel.self) { [weak self] result in
            switch result {
            case let .success(executeModel):
                self?.setupUser(executeModel)
                complition(.success(nil))
            case let .reject(error):
                complition(.reject(error))
            }
        }
    }
    
    func getAllAdherences(completion: @escaping ExecuteCompletion<ExecuteAdherencesModel>) {
        guard let user = user else {
            return
        }
        
        let endPoint = HealtheraEndPoint.adherences(user)
        apiEngine.execute(for: endPoint, model: ExecuteAdherencesModel.self, completion)
    }
    
    func getRemedy(with id: String, completion: @escaping ExecuteCompletion<ExecuteRemedyModel>) {
        guard let user = user else {
            return
        }
        
        let remedy = RemedyModel(user: user, remedyId: id)
        let endPoint = HealtheraEndPoint.remedy(remedy)
        apiEngine.execute(for: endPoint, model: ExecuteRemedyModel.self, completion)
    }
    
    func logout(completion: @escaping ExecuteCompletion<Void?>) {
        guard let user = user else {
            return
        }
       
        let endPoint = HealtheraEndPoint.logout(user)
        apiEngine.execute(for: endPoint, model: LoguotModel.self) { [weak self] result in
            switch result {
            case let .success(model):
                print(model)
                self?.user = nil
                completion(.success(nil))
            case let .reject(error):
                completion(.reject(error))
            }
        }
    }

    private func setupUser(_ loginModel: LoginModel) {
        let jwtToken = loginModel.data.isEmpty ? "" : loginModel.data[0].token
        user = AppUser(jwtToken: jwtToken,
                       clientId: loginModel.aux.tokenPayload.userId,
                       appVersion: Constant.mockAppVersion,
                       patientId: loginModel.aux.tokenPayload.userId)
        
    }
}
