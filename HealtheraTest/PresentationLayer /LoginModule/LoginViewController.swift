//
//  LoginViewController.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/20/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import UIKit

protocol LoginViewControllerProtocol: AnyObject {
    func handle(error: DisplayError)
    func startAppCoordinator()
    func setupUserInteraction()
}

class LoginViewController: UIViewController {
    var presenter: LoginPresenterProtocol?
    private var imageView = UIImageView()
    
    private var usernameTextField = TextField {
        $0.title = "Username".uppercased()
        $0.textFieldType = .email
    }
    
    private var passwordTextField = TextField {
        $0.title = "Password".uppercased()
        $0.textFieldType = .password
    }
    
    private var loginButton = LoadingButton {
        $0.layer.cornerRadius = 10
        $0.backgroundColor = .customGreen
        $0.setTitle("Log in", for: .normal)
        $0.setTitleColor(.white, for: .normal)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    @objc func didTapLogin() {
        usernameTextField.validateField()
        passwordTextField.validateField()
        guard usernameTextField.validateField(),
            passwordTextField.validateField(),
            let password = passwordTextField.text,
            let username = usernameTextField.text else {
                return
        }
        view.isUserInteractionEnabled = false
        loginButton.loading(true)
        presenter?.didTapLogin(username: username, password: password)
    }
    
    private func setupUI() {
        let stackView = UIStackView {
            $0.axis = .vertical
            $0.spacing = 20
        }
        view.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.addArrangedSubviews([imageView, usernameTextField, passwordTextField])
        
        view.addSubview(loginButton)
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        loginButton.addTarget(self, action: #selector(didTapLogin), for: .touchDown)
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: stackView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 100),
            NSLayoutConstraint(item: stackView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 40),
            NSLayoutConstraint(item: stackView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: -40),
            NSLayoutConstraint(item: loginButton, attribute: .top, relatedBy: .equal, toItem: stackView, attribute: .bottom, multiplier: 1, constant: 200),
            NSLayoutConstraint(item: loginButton, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 15),
            NSLayoutConstraint(item: loginButton, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: -15),
            NSLayoutConstraint(item: loginButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50),
            NSLayoutConstraint(item: loginButton, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: -50)
        ])
    }
}

extension LoginViewController: LoginViewControllerProtocol {
    func setupUserInteraction() {
        DispatchQueue.main.async { [weak self] in
            self?.view.isUserInteractionEnabled = false
            self?.loginButton.loading(true)
        }
    }
}
