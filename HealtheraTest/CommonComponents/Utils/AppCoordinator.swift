//
//  AppCoordinator.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/20/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import UIKit

class AppCoordinator {
    private let userService: UserServiceProtocol
    let window: UIWindow?
    
    init(window: UIWindow?) {
        self.userService = ModuleBuilder.buildUserService()
        self.window = window
    }
    
    func start() {
        userService.shouldAuthorize ? loginFlow() : mainFlow()
    }
    
    func loginFlow() {
        let loginViewController = ModuleBuilder.buildLoginModule(userService: userService)
        window?.rootViewController = loginViewController
        window?.makeKeyAndVisible()
    }
    
    func mainFlow() {
        let vc = ModuleBuilder.buildAdherencesPagesModule()
        window?.rootViewController = UINavigationController(rootViewController: vc)
        window?.makeKeyAndVisible()
    }
}
