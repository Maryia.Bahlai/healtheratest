//
//  UIColor+Extension.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/21/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import UIKit

extension UIColor {
    static let customGreen = UIColor.init(red: 23.0/255, green: 197.0/255, blue: 157.0/255, alpha: 1.0)
}
