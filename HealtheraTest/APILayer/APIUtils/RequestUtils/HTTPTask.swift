//
//  HTTPTask.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/19/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

enum HTTPTask {
    case requestAdditionHeaders(additionHeaders: HTTPHeaders?)
    
    case requestAdditionHeadersAndBody(bodyParameters: Parameters,
                                       additionHeaders: HTTPHeaders?,
                                       bodyEncoding: ParameterEncoding)
}
