//
//  UIViewController+Extension.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/20/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import UIKit

extension UIViewController {
    func handle(error: DisplayError) {
        DispatchQueue.main.async { [weak self] in
            AlertDispatcher().showAlert(self, with: nil, and: error.message)
        }
    }
    
    func startAppCoordinator() {
        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.appCoordinator?.start()
    }
}

