//
//  AdherencePresenter.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/21/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

protocol AdherencePresenterProtocol {
    func getSchedule()
}

class AdherencePresenter: AdherencePresenterProtocol {
    private weak var view: AdherenceViewControllerProtocol?
    private var userService: UserServiceProtocol
     private var adherenceModel: AdherenceModel
    
    init(userService: UserServiceProtocol,
         view: AdherenceViewControllerProtocol?,
         adherenceModel: AdherenceModel) {
        self.userService = userService
        self.view = view
        self.adherenceModel = adherenceModel
    }
    
    func getSchedule() {
        userService.getRemedy(with: adherenceModel.remedy_id) { [weak self] result in
            //TODO
        }
    }
}
