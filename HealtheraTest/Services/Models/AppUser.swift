//
//  AppUser.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/19/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

struct AppUser: Codable {
    var jwtToken: String
    var clientId: String
    var appVersion: String
    var patientId: String
    var start: Int?
    var end: Int?
    
    init(jwtToken: String, clientId: String, appVersion: String, patientId: String) {
        self.jwtToken = jwtToken
        self.clientId = clientId
        self.appVersion = appVersion
        self.patientId = patientId
    }
}
