//
//  ErrorHandler.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/19/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

protocol ErrorHandlerProtocol {
    func handle(_ error: Error) -> DisplayError
    func handle(_ error: ExecuteErrorModel) -> DisplayError
    func handle(_ response: HTTPURLResponse) -> DisplayError
}

struct ErrorHandler: ErrorHandlerProtocol {
    func handle(_ error: Error) -> DisplayError {
        switch (error as NSError).code {
        case NSURLErrorNotConnectedToInternet, NSURLErrorNetworkConnectionLost:
            return NetworkError.noConnection
        default:
            return CustomError(message: error.localizedDescription)
        }
    }
    
    func handle(_ errorModel: ExecuteErrorModel) -> DisplayError {
        return CustomError(message: errorModel.error.text)
    }
    
    func handle(_ response: HTTPURLResponse) -> DisplayError {
        switch response.statusCode {
        case 401:
            return NetworkError.unauthorized
        case 404:
            return NetworkError.notFound
        case 403:
            return NetworkError.unauthorized
        case (500...599):
            return NetworkError.serverError
        default:
            return CustomError(message: "unknow")
        }
    }
}
