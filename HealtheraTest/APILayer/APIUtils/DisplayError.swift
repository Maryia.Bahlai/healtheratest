//
//  NetworkError.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/19/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

protocol DisplayError: Error {
    var message: String { get }
}

struct CustomError: DisplayError {
    var message: String
    
    init(message: String) {
        self.message = message
    }
}

enum NetworkError: DisplayError {
    case encodingFailed
    case missingURL
    case noConnection
    case unauthorized
    case notFound
    case serverError
    
    var message: String {
        switch self {
        case .encodingFailed:
            return "Parameter encoding failed"
        case .missingURL:
            return "URL is nil"
        case .noConnection:
            return "No connecton to the internet"
        case .unauthorized:
            return "Unauthorized user"
        case .notFound:
            return "Server not found"
        case .serverError:
            return "Server error"
        }
    }
}
