//
//  ExecuteErrorModel.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/20/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

struct ExecuteErrorModel: Codable, Error {
    let error: ErrorModel
}

struct ErrorModel: Codable {
    let code: String
    let text: String
}
