//
//  AdherencesPageViewController.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/20/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import UIKit

protocol AdherencesPageViewControllerProtocol: AnyObject {
    func handle(error: DisplayError)
    func setSlides(_ slides: [AdherenceViewController])
    func showNextSlide()
    func showPreviousSlide()
    func startAppCoordinator()
    func setupUserInteraction()
}

class AdherencesPageViewController: UIPageViewController {
    var presenter: AdherencesPagePresenterProtocol?
    
    private var slides = [AdherenceViewController]()
    
    private var currentIndex: Int {
        get {
            guard let vc = viewControllers?.first as? AdherenceViewController,
                let index = slides.firstIndex(of: vc)
            else {
                return 0
            }
            return index
        }
        set {
            guard newValue >= 0,
                newValue < slides.count
            else {
                return
            }
            let vc = slides[newValue]
            let direction = newValue > currentIndex ?
                UIPageViewController.NavigationDirection.forward :
                UIPageViewController.NavigationDirection.reverse
            setViewControllers([vc], direction: direction, animated: true) { completed in
                self.delegate?.pageViewController?(self,
                                                   didFinishAnimating: true,
                                                   previousViewControllers: [],
                                                   transitionCompleted: completed)
            }
        }
    }
    
    private lazy var activityBarItem = UIBarButtonItem(customView: activityIndicator)
    
    var isLoading: Bool = false {
        didSet {
            if isLoading {
                view.isUserInteractionEnabled = false
                activityIndicator.startAnimating()
                navigationItem.rightBarButtonItem = activityBarItem
            } else {
                view.isUserInteractionEnabled = true
                activityIndicator.stopAnimating()
                navigationItem.rightBarButtonItem = nil
            }
        }
    }

    private lazy var activityIndicator = UIActivityIndicatorView {
        $0.hidesWhenStopped = true
        $0.style = UIActivityIndicatorView.Style.medium
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Schedule"
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Log out", style: .plain, target: self, action: #selector(didTapLogout))

        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barTintColor = .customGreen
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        
        setupCommonUI()
        
        presenter?.getAllAdherences()
    }
    
    @objc func didTapLogout() {
        isLoading = true
        presenter?.loguot()
    }

    private func setupCommonUI() {
        dataSource = self
        view.backgroundColor = .lightGray
        if let firstViewController = slides.first {
            setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)
        }
    }
}

extension AdherencesPageViewController: UIPageViewControllerDataSource {
    func presentationCount(for _: UIPageViewController) -> Int {
        return slides.count
    }

    func presentationIndex(for _: UIPageViewController) -> Int {
        return currentIndex
    }

    func pageViewController(_: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let slide = viewController as? AdherenceViewController, let index = slides.firstIndex(of: slide) else {
            return nil
        }
        return index > 0 ? slides[index - 1] : nil
    }

    func pageViewController(_: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let slide = viewController as? AdherenceViewController, let index = slides.firstIndex(of: slide) else {
            return nil
        }
        let lastIndex = slides.count - 1
        if index < lastIndex {
            return slides[index + 1]
        }
        return nil
    }
}


extension AdherencesPageViewController: AdherencesPageViewControllerProtocol {
     func setSlides(_ slides: [AdherenceViewController]) {
        self.slides = slides
    }
        
    func showNextSlide() {
        currentIndex += 1
    }
        
    func showPreviousSlide() {
        currentIndex -= 1
    }
    
    func setupUserInteraction() {
        DispatchQueue.main.async { [weak self] in
            self?.isLoading = false
        }
    }
}
