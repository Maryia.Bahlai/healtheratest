//
//  RequestBuilder.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/19/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

protocol RequestBuilderProtocol {
    func buildRequest<EndPoint: HTTPEndPoint>(for endPoint: EndPoint) throws -> URLRequest
}

struct RequestBuilder: RequestBuilderProtocol {
    func buildRequest<EndPoint: HTTPEndPoint>(for endPoint: EndPoint) throws -> URLRequest {
        var request = URLRequest(url: endPoint.baseURL.appendingPathComponent(endPoint.path),
                                 cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                                 timeoutInterval: 10.0)
        
        request.httpMethod = endPoint.httpMethod.rawValue
        
        do {
            switch endPoint.task {
            case let .requestAdditionHeadersAndBody(bodyParameters: bodyParameters,
                                                  additionHeaders: additionHeaders,
                                                  bodyEncoding: bodyEncoding):
                
                try configureBody(bodyParameters: bodyParameters,
                                  bodyEncoding: bodyEncoding,
                                  request: &request)
                
                configureHeaders(additionHeaders, request: &request)
            case let .requestAdditionHeaders(additionHeaders: additionHeaders):
                 configureHeaders(additionHeaders, request: &request)
            }
            return request
        } catch {
            throw error
        }
    }
    
    private func configureHeaders(_ additionalHeaders: HTTPHeaders?,
                                  request: inout URLRequest) {
        if request.value(forHTTPHeaderField: HTTPHeaderFields.contentType) == nil {
            let mediaType = MediaTypeGenerator.generate(type: .application, subtype: .json)
            request.setValue(mediaType, forHTTPHeaderField: HTTPHeaderFields.contentType)
            request.setValue(mediaType, forHTTPHeaderField: HTTPHeaderFields.accept)
        }
        guard let headers = additionalHeaders else { return }
        for (key, value) in headers {
            request.setValue(value, forHTTPHeaderField: key)
        }
    }
    
    private func configureBody(bodyParameters: Parameters?,
                               bodyEncoding: ParameterEncoding,
                               request: inout URLRequest) throws {
        do {
            try bodyEncoding.encode(urlRequest: &request,
                                    bodyParameters: bodyParameters)
        } catch {
            throw error
        }
    }
}
