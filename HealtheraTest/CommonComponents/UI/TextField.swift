//
//  TextField.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/20/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import UIKit

class TextField: UITextField {

    static var tagForController = 0
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: bounds.size.width, height: titleHeight() + textHeight())
    }
    
    override var isSecureTextEntry: Bool {
        set {
            super.isSecureTextEntry = newValue
            fixCaretPosition()
        }
        get {
            return super.isSecureTextEntry
        }
    }
    
    override var text: String? {
        didSet {
            updateControl(false)
        }
    }
        
    var title: String? {
        didSet {
            updateControl()
        }
    }
    
    var textFieldType = TextFieldType.none {
        willSet {
            switch newValue {
                case .password:
                    isSecureTextEntry = true
                default:
                    return
            }
        }
    }

    private var lineHeight: CGFloat = 1.0 {
        didSet {
            setNeedsDisplay()
        }
    }

    var errorMessage: String? {
        didSet {
            updateControl(true)
        }
    }
    
    private var hasErrorMessage: Bool {
        return errorMessage != nil && errorMessage != ""
    }
    
    private var lineView: UIView!
    private var titleLabel: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)
        initTextField()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initTextField()
    }

    private func initTextField() {
        tag = TextField.tagForController
        TextField.tagForController += 1
        returnKeyType = .next
        borderStyle = .none
        createTitleLabel()
        createLineView()
        addEditingChangedObserver()
        updatePlaceholder()
        updateControl()
    }

    fileprivate func addEditingChangedObserver() {
        addTarget(self, action: #selector(TextField.editingChanged), for: .editingChanged)
        addTarget(self, action: #selector(TextField.editingDidBegin), for: .editingDidBegin)
        addTarget(self, action: #selector(TextField.editingDidEnd), for: .editingDidEnd)
    }

    @objc public func editingChanged(_ sender: Any) {
        errorMessage = nil
        updateControl(true)
        updateTitleLabel(true)
    }

    private func createTitleLabel() {
        titleLabel = UILabel()
        titleLabel.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        titleLabel.font = UIFont.systemFont(ofSize: 14)
        titleLabel.alpha = 1
        titleLabel.textColor = .black

        addSubview(titleLabel)
        titleLabel.frame = CGRect(x: 0, y: -10, width: bounds.size.width, height: titleHeight())
    }

    private func createLineView() {
        lineView = UIView()
        lineView.isUserInteractionEnabled = false
        
        lineView.backgroundColor = .black
        lineView.frame = CGRect(x: 0, y: bounds.size.height - 1, width: bounds.size.width, height: 1)
        addSubview(lineView)
        let onePixel: CGFloat = 1.0 / UIScreen.main.scale
        lineHeight = 2.0 * onePixel
    }


    // MARK: - View updates
    
    private func updatePlaceholder() {
        guard let placeholder = placeholder, let font =  font else {
            return
        }
        let color = UIColor.gray
        #if swift(>=4.0)
            attributedPlaceholder = NSAttributedString(string: placeholder,
                                                       attributes: [NSAttributedString.Key.foregroundColor: color, NSAttributedString.Key.font: font])
        #else
            attributedPlaceholder = NSAttributedString(string: placeholder,
                                                       attributes: [NSForegroundColorAttributeName: color, NSFontAttributeName: font])
        #endif
    }


    private func updateControl(_ animated: Bool = false) {
        updateTitleColor()
        updateTitleLabel(animated)
    }

    private func updateTitleColor() {
        if hasErrorMessage {
            titleLabel.textColor = .red
        } else {
            titleLabel.textColor = .black
        }
    }

    private func updateTitleLabel(_ animated: Bool = false) {
        var titleText: String?
        if hasErrorMessage {
            titleText = errorMessage!
        } else {
            titleText = title
        }
        titleLabel.text = titleText
    }
    
    func titleHeight() -> CGFloat {
        if let titleLabel = titleLabel,
            let font = titleLabel.font {
            return font.lineHeight
        }
        return 15.0
    }

    func textHeight() -> CGFloat {
        return font!.lineHeight + 7.0
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        titleLabel.frame = CGRect(x: 0, y: -10, width: bounds.size.width, height: titleHeight())
        lineView.frame = CGRect(x: 0, y: bounds.size.height - 1, width: bounds.size.width, height: 1)
    }

    open func fixCaretPosition() {
        let beginning = beginningOfDocument
        selectedTextRange = textRange(from: beginning, to: beginning)
        let end = endOfDocument
        selectedTextRange = textRange(from: end, to: end)
    }

    @objc public func editingDidBegin(_ sender: Any) {
        errorMessage = nil
        layoutIfNeeded()
    }
    
    @objc public func editingDidEnd(_ sender: Any) {
        if text != "" {
            validateField()
        }
    }
    
    @discardableResult public func validateField() -> Bool {
        Validator(self).validateField()
    }
    
    @discardableResult override func becomeFirstResponder() -> Bool {
        let result = super.becomeFirstResponder()
        updateControl(true)
        return result
    }

    @discardableResult override func resignFirstResponder() -> Bool {
        let result = super.resignFirstResponder()
        updateControl(true)
        return result
    }
    
    func textFieldShouldReturn(_ complition: @escaping () -> Void) -> Bool {
        if let nextField = superview?.viewWithTag(tag + 1) as? UITextField {
            var count = 0
            superview?.subviews.forEach { view in
                if view is TextField {
                    count += 1
                }
            }
            guard nextField.tag == count - 1 else {
                nextField.becomeFirstResponder()
                return false
            }
            nextField.returnKeyType = .done
            nextField.becomeFirstResponder()
        } else {
            resignFirstResponder()
            complition()
        }
        return false
    }
}

class Validator {

    private var textField: TextField

    private var text: String? {
        textField.text
    }

    private var textFieldType: TextFieldType {
        textField.textFieldType
    }

    private var errorMessage: String? {
        get {
            textField.errorMessage
        }

        set {
            textField.errorMessage = newValue
        }
    }

    init(_ textField: TextField) {
        self.textField = textField
    }

    func validateField() -> Bool {
        guard let text = text else {
            return false
        }
        switch textFieldType {
        case .email:
            if !text.isValidEmail {
                errorMessage = "Please enter a valid username."
                return false
            }
        case .password:
            if text.count < 6 {
                errorMessage = "Password field must be at least 6 characters."
                return false
            }
        case .none:
            break
        }
        return true
    }
}

enum TextFieldType {
    case email
    
    case password
    
    case none
}
