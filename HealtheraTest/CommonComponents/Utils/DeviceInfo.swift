//
//  DeviceInfo.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/19/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import UIKit

struct DeviceInfo {
    static func getDeviceToken() -> String? {
        return UIDevice.current.identifierForVendor?.uuidString
    }
    
    static func getDevicePlatform() -> String {
        return UIDevice.current.systemName
    }
}
