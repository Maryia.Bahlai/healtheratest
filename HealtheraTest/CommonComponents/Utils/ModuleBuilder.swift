//
//  ModuleBuilder.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/20/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import UIKit

struct ModuleBuilder {
    static func buildAdherencesModule(with adherenceModel: AdherenceModel, previousAction: (()->())?, nextAction: (()->())?) -> AdherenceViewController {
        let view = AdherenceViewController(previousAction: previousAction, nextAction: nextAction)
        let presenter = AdherencePresenter(userService: buildUserService(), view: view, adherenceModel: adherenceModel)
        view.presenter = presenter
        return view
        
    }
    
    static func buildAdherencesPagesModule() -> UIViewController {
        let view = AdherencesPageViewController()
        let presenter = AdherencesPagePresenter(userService: buildUserService(), view: view)
        view.presenter = presenter
        return view
    }
    
    static func buildLoginModule(userService: UserServiceProtocol) -> UIViewController {
        let view = LoginViewController()
        let presenter = LoginPresenter(userService: buildUserService(),
                                       view: view)
        view.presenter = presenter
        return view
    }
    
    static func buildUserService() -> UserServiceProtocol {
        let requestBiuilder = RequestBuilder()
        let executer = Executor()
        let errorHandler = ErrorHandler()
        let parser = Parser()
        let apiEngine = APIEngine(requestBuilder: requestBiuilder,
                                  executor: executer,
                                  errorHandler: errorHandler,
                                  parser: parser)
        return UserService(apiEngine)
    }
}
