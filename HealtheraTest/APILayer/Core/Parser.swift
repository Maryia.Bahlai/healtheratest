//
//  Parser.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/19/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

protocol ParserProtocol {
    func parseData<Model: Decodable>(_ data: Data, with decoder: DecoderType, to model: Model.Type) throws -> Model
}

struct Parser: ParserProtocol {
    func parseData<Model: Decodable>(_ data: Data, with decoder: DecoderType, to model: Model.Type) throws -> Model {
        do {
            return try decoder.decode(model, from: data)
        } catch {
            throw error
        }
    }
}
