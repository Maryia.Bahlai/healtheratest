//
//  JSONParameterEncoder.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/19/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

struct JSONParameterEncoder {
    static func encode(urlRequest: inout URLRequest, with parameters: Parameters) throws {
        do {
            let jsonAsData = try JSONSerialization.data(withJSONObject: parameters,
                                                        options: .prettyPrinted)
            urlRequest.httpBody = jsonAsData
        } catch {
            throw NetworkError.encodingFailed
        }
    }
}
