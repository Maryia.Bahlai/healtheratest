//
//   ExecuteCompletion.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/19/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

typealias ExecuteCompletion<Type> = (ExecuteModel<Type>) -> Void

enum ExecuteModel<Type> {
    case success(Type)
    case reject(DisplayError)
}
