//
//  AlertDispatcher.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/20/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import UIKit

class AlertDispatcher {
    func present(_ viewController: UIViewController?, alert: UIAlertController) {
        viewController?.present(alert, animated: true, completion: nil)
    }

    func showAlert(_ viewController: UIViewController?, with title: String?, and message: String?, complition: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let cancelAction = UIAlertAction(title: "Ok", style: .default) { _ in
            complition?()
        }
        alert.addAction(cancelAction)
        DispatchQueue.main.async {
            viewController?.present(alert, animated: true, completion: nil)
        }
    }
}

