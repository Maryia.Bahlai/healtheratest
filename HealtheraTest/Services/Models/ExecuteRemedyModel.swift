//
//  RemedyModel.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/20/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

struct ExecuteRemedyModel: Codable {
    let data: [RemedyDataModel]
}

struct RemedyDataModel: Codable {
    let patient_id: String
    let remedy_id: String
    let course_quantity: Int
    let date_created: TimeInterval
    let date_modified: TimeInterval
    let dispense_id: String
    let dose_continuous: String?
    let dose_discrete: String
    let medicine_type: String
    let instruction: String
    let medicine_name: String
    let oba: String
    let pharmacy_id: String
    let reorder_timestamp: TimeInterval
    let restrictions: String
    let schedule: [RemedyScheduleModel]
    let special_instruction: String
    let strictness: Int
    let taker_num: Int
}

struct RemedyScheduleModel: Codable {
    let day_offset: Int
    let day_iterator: Int
    let dose_min: Int
    let dose_max: Int
    let alarm_window: String
    let number_of_alarms: Int
    let dose_string: String
    let number_of_loops: Int
}
