//
//  AdherencesPagePresenter.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/20/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

protocol AdherencesPagePresenterProtocol  {
    func getAllAdherences()
    func loguot()
}

class AdherencesPagePresenter: AdherencesPagePresenterProtocol {
    private weak var view: AdherencesPageViewControllerProtocol?
    private var userService: UserServiceProtocol
    
    init(userService: UserServiceProtocol,
         view: AdherencesPageViewControllerProtocol?) {
        self.userService = userService
        self.view = view
    }
    
    func getAllAdherences() {
        userService.getAllAdherences { [weak self] result in
            switch result {
            case let .success(model):
                DispatchQueue.main.async {
                    let slides = model.data.map{ ModuleBuilder.buildAdherencesModule(with: $0, previousAction: { [weak self] in
                        self?.view?.showPreviousSlide()
                    }) { [weak self] in
                        self?.view?.showNextSlide()
                    } }
                    self?.view?.setSlides(slides)
                }
            case let .reject(error):
                self?.view?.handle(error: error)
                self?.view?.setupUserInteraction()
            }
        }
    }
    
    func loguot() {
        userService.logout { [weak self] result in
            switch result {
            case .success:
                DispatchQueue.main.async {
                    self?.view?.startAppCoordinator()
                }
            case let .reject(error):
                self?.view?.handle(error: error)
                self?.view?.setupUserInteraction()
            }
        }
    }
}
