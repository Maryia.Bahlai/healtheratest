//
//  RemedyModel.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/20/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

struct RemedyModel {
    let user: AppUser
    let remedyId: String
}
