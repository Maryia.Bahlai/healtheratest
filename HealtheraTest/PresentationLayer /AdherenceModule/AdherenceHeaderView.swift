//
//  AdherenceHeaderView.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/21/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import UIKit

class AdherenceHeaderView: UIView {
    var dateText: String? {
        didSet {
            dateLabel.text = dateText
        }
    }
    
    var descriptionText: String? {
        didSet {
            descriptionLabel.text = descriptionText
        }
    }
    
    var image: UIImage? {
        didSet {
            imageView.image = image
        }
    }
    
    var previousButtonAction: (()->())?
    var nextButtonAction: (()->())?
    
    private var dateLabel = UILabel {
        $0.font = UIFont.boldSystemFont(ofSize: 18)
    }
    
    private var descriptionLabel = UILabel {
        $0.textColor = .orange
        $0.font = UIFont.systemFont(ofSize: 16)
    }
    
    private var imageView = UIImageView {
        $0.contentMode = .scaleAspectFit
        $0.image = UIImage(named: "sunny")
    }
    
    private var nextButton = UIButton {
        $0.setTitle(">", for: .normal)
        $0.setTitleColor(.black, for: .normal)
        $0.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
    }
    
    private var previousButton = UIButton {
        $0.setTitle("<", for: .normal)
        $0.setTitleColor(.black, for: .normal)
        $0.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
    }
    
    private let verticalStackView = UIStackView {
        $0.axis = .vertical
        $0.spacing = 20
        $0.distribution = .equalSpacing
        $0.alignment = .center
    }
    
    private let horizontalStackView = UIStackView {
        $0.axis = .horizontal
        $0.spacing = 5
        $0.distribution = .equalSpacing
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        horizontalStackView.addArrangedSubviews([previousButton, dateLabel, nextButton])
        previousButton.addTarget(self, action: #selector(didTapPreviousButton), for: .touchDown)
        nextButton.addTarget(self, action: #selector(didTapNextButton), for: .touchDown)
        addSubview(verticalStackView)
        verticalStackView.translatesAutoresizingMaskIntoConstraints = false
        verticalStackView.addArrangedSubviews([horizontalStackView, descriptionLabel, imageView])
        
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: verticalStackView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 20),
            NSLayoutConstraint(item: verticalStackView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: verticalStackView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: verticalStackView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: -20),
            NSLayoutConstraint(item: horizontalStackView, attribute: .width, relatedBy: .equal, toItem: verticalStackView, attribute: .width, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: imageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 150)
        ])
    }
    
    @objc func didTapPreviousButton() {
        previousButtonAction?()
    }
    
    @objc func didTapNextButton() {
        nextButtonAction?()
    }
}


