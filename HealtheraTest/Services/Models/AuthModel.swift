//
//  AuthModel.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/19/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

struct AuthModel: Codable {
    let username: String
    let password: String
    var clientId: String
    let deviceToken: String?
}
