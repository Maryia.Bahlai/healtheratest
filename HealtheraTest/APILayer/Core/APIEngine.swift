//
//  APIEngine.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/19/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation


protocol APIEngineProtocol {
    func execute<EndPoint: HTTPEndPoint, Model: Codable>(for endPoint: EndPoint,
                                                           model: Model.Type,
                                                           _ completion: @escaping ExecuteCompletion<Model>)
}

final class APIEngine: APIEngineProtocol {
    private var requestBuilder: RequestBuilderProtocol
    private var executor: ExecutorProtocol
    private var errorHandler: ErrorHandlerProtocol
    private var parser: ParserProtocol
    
    init(requestBuilder: RequestBuilderProtocol,
         executor: ExecutorProtocol,
         errorHandler: ErrorHandlerProtocol,
         parser: ParserProtocol) {
        self.requestBuilder = requestBuilder
        self.executor = executor
        self.errorHandler = errorHandler
        self.parser = parser
    }
    
    func execute<EndPoint: HTTPEndPoint, Model: Codable>(for endPoint: EndPoint,
                                                           model: Model.Type,
                                                           _ completion: @escaping ExecuteCompletion<Model>) {
        DispatchQueue.networkQueue().async { [weak self] in
            guard let self = self else {
                return
            }
            
            do {
                let request = try self.requestBuilder.buildRequest(for: endPoint)
                self.execute(request, model: model, completion)
            } catch {
                completion(.reject(NetworkError.missingURL))
            }
        }
    }
    
    private func execute<Model: Codable>(_ request: URLRequest,
                                        model: Model.Type,
                                        _ completion: @escaping ExecuteCompletion<Model>) {
        executor.execute(request: request, errorHandler: errorHandler) { [weak self] result  in
            guard let self = self else {
                return
            }
            switch result {
            case let .success(data):
                do {
                    if let parsedData = try? self.parseData(
                    data,
                    model: model,
                    decoder: JSONDecoder()) {
                        completion(.success(parsedData))
                    } else {
                        let error = try self.parseData(
                        data,
                        model: ExecuteErrorModel.self,
                        decoder: JSONDecoder())
                        completion(.reject(self.errorHandler.handle(error)))
                    }
                } catch {
                    completion(.reject(self.errorHandler.handle(error)))
                }
            case let .reject(error):
                completion(.reject(error))
            }
        }
    }
    
    private func parseData<Model: Codable>(_ data: Data, model: Model.Type, decoder: DecoderType) throws -> Model {
        do {
            return try parser.parseData(data, with: decoder, to: model)
        } catch {
            throw error
        }
    }
}
