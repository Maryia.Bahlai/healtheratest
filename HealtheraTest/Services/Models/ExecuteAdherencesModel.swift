//
//  AllAdherences.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/20/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

struct ExecuteAdherencesModel: Codable {
    let data: [AdherenceModel]
}

struct AdherenceModel: Codable {
    let patient_id: String
    let remedy_id: String
    let alarm_time: TimeInterval
    let adherence_id: String
    let action: String
    let action_time: TimeInterval
    let dose_discrete: Double?
    let dose_quantity: Int
    let note: String?
    let date_modified: TimeInterval
}
