//
//  MediaTypeGenerator.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/19/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

struct MediaTypeGenerator {
    static func generate(type: Type,
                         subtype: Subtype,
                         parameter: Parameters? = nil) -> String {
        return type.rawValue + "/" + subtype.rawValue
    }
}

enum Type: String {
    case application = "application"
}

enum Subtype: String {
    case json = "json"
}
