//
//  DispatchQueue+Extension.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/19/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

extension DispatchQueue {
    static func networkQueue() -> DispatchQueue {
        return DispatchQueue(label: "com.HealtheraTest.NetworkQueue", qos: .userInitiated, attributes: .concurrent, autoreleaseFrequency: .inherit)
    }
}
