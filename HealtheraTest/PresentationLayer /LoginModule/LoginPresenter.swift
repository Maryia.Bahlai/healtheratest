//
//  LoginPresenter.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/20/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

protocol LoginPresenterProtocol {
     func didTapLogin(username: String, password: String)
}

class LoginPresenter: LoginPresenterProtocol {
    private weak var view: LoginViewControllerProtocol?
    private var userService: UserServiceProtocol
    
    init(userService: UserServiceProtocol,
         view: LoginViewControllerProtocol?) {
        self.userService = userService
        self.view = view
    }
    
    func didTapLogin(username: String, password: String) {
        userService.login(username: username, password: password) { [weak self] result in
            switch result {
            case .success:
                DispatchQueue.main.async {
                    self?.view?.startAppCoordinator()
                }
            case let .reject(error):
                self?.view?.handle(error: error)
                self?.view?.setupUserInteraction()
            }
        }
    }
}
