//
//  ParameterEncoding.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/19/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

typealias Parameters = [String: Any]

enum ParameterEncoding {
    case jsonEncoding
    
    func encode(urlRequest: inout URLRequest,
                bodyParameters: Parameters?) throws {
        do {
            switch self {
            case .jsonEncoding:
                guard let bodyParameters = bodyParameters else { return }
                try JSONParameterEncoder.encode(urlRequest: &urlRequest, with: bodyParameters)
            }
        } catch {
            throw error
        }
    }
}
