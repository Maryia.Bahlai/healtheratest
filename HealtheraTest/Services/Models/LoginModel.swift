//
//  LoginModel.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/19/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

struct LoginModel: Codable {
    let data: [JWTTokenModel]
    let aux: AuxModel
}

struct JWTTokenModel: Codable {
    let token: String
}

struct AuxModel: Codable {
    let tokenPayload: UserTokenModel
}

struct UserTokenModel: Codable {
    let userId: String
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: JSONUserTokenModelKeys.self)
        userId = try container.decode(String.self, forKey: .userId)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: JSONUserTokenModelKeys.self)
        try container.encode(userId, forKey: .userId)
    }
}

extension UserTokenModel {
    enum JSONUserTokenModelKeys: String, CodingKey {
        case userId = "user_id"
    }
}
