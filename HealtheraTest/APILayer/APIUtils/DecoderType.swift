//
//  DecoderType.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/19/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

protocol DecoderType {
    func decode<T: Decodable>(_ type: T.Type, from data: Data) throws -> T
}

extension JSONDecoder: DecoderType {}
