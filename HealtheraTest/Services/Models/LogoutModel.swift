//
//  LogoutModel.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/19/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

struct LoguotModel: Codable {
    let data: [LogoutToken]
}

struct LogoutToken: Codable {
    let tokenId: String
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: JSONLogoutTokenKeys.self)
        tokenId = try container.decode(String.self, forKey: .tokenId)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: JSONLogoutTokenKeys.self)
        try container.encode(tokenId, forKey: .tokenId)
    }
}

extension LogoutToken {
    enum JSONLogoutTokenKeys: String, CodingKey {
        case tokenId = "token_id"
    }
}
