//
//  RemedyTableViewCell.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/20/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import UIKit

class RemedyTableViewCell: UITableViewCell {
    override var frame: CGRect {
        get { return super.frame }
        set {
            let padding = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
            super.frame = newValue.inset(by: padding)
        }
    }
    
    private var remedyImageView = UIImageView {
        $0.backgroundColor = .customGreen
        $0.contentMode = .scaleAspectFit
    }
    
    private var timeLable = UILabel {
        $0.textColor = .darkGray
        $0.font = UIFont.systemFont(ofSize: 19)
    }
    
    private var descriptionLabel = UILabel {
        $0.numberOfLines = 0
        $0.textColor = .darkGray
        $0.font = UIFont.systemFont(ofSize: 16)
    }
    
    private var statusLabel = UILabel {
        $0.textColor = .customGreen
    }
    
    private var verticalStackView = UIStackView {
        $0.axis = .vertical
        $0.spacing = 10
    }
    
    private var horizontalStackView = UIStackView {
        $0.axis = .horizontal
        $0.distribution = .equalSpacing
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }

    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        remedyImageView.image = nil
        timeLable.text = nil
        descriptionLabel.text = nil
        statusLabel.text = nil
    }
    
    func update() {
        prepareForReuse()
        timeLable.text = "10.00 AM"
        descriptionLabel.text = "Potassium, 1 Tablets"
        statusLabel.text = "Taken"
    }
    
    private func setupViews() {
        selectionStyle = .none
        backgroundColor = .white
        layer.cornerRadius = bounds.height / 3
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOpacity = 2
        layer.shadowRadius = 10
        layer.shadowOffset = CGSize.zero
        
        addSubview(remedyImageView)
        remedyImageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: remedyImageView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 15),
            NSLayoutConstraint(item: remedyImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30),
            NSLayoutConstraint(item: remedyImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 30),
            NSLayoutConstraint(item: remedyImageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        ])
        
        horizontalStackView.addArrangedSubviews([timeLable, statusLabel])
        addSubview(verticalStackView)
        verticalStackView.translatesAutoresizingMaskIntoConstraints = false
        verticalStackView.addArrangedSubviews([horizontalStackView, descriptionLabel])
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: verticalStackView, attribute: .leading, relatedBy: .equal, toItem: remedyImageView, attribute: .trailing, multiplier: 1, constant: 10),
            NSLayoutConstraint(item: verticalStackView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 20),
            NSLayoutConstraint(item: verticalStackView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -10),
            NSLayoutConstraint(item: verticalStackView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: -20)
        ])
    }
}
