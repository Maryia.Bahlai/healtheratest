//
//  RemedyView.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/21/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import UIKit

class RemedyView: UIView {
    private var dimmView = UIView {
        $0.backgroundColor = .black
        $0.alpha = 0.8
    }
    
    private var contentView = UIView {
        $0.backgroundColor = .white
        $0.clipsToBounds = true
    }
    
    private var headerView = UIView {
        $0.backgroundColor = .customGreen
    }
    
    private var labelsStackView = UIStackView {
        $0.axis = .vertical
        $0.alignment = .center
    }
    
    private var dateLabel = UILabel {
        $0.textColor = .white
        $0.font = UIFont.systemFont(ofSize: 14)
    }
    
    private var descriptionLabel = UILabel {
        $0.textColor = .white
        $0.font = UIFont.systemFont(ofSize: 14)
    }
    
    private var statusLabel = UILabel {
        $0.textColor = .customGreen
        $0.font = UIFont.systemFont(ofSize: 16)
    }
    
    required init(frame: CGRect, dateText: String?, descriptionText: String?, statusText: String?) {
        super.init(frame: frame)
        dateLabel.text = dateText
        descriptionLabel.text = descriptionText
        statusLabel.text = statusText
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
        
    private func setupView() {
        
        addSubview(contentView)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: contentView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: contentView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: contentView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 130),
            NSLayoutConstraint(item: contentView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 370)
        ])
        contentView.layer.cornerRadius = frame.height / 40
        
        contentView.addSubview(headerView)
        headerView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: headerView, attribute: .top, relatedBy: .equal, toItem: contentView, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: headerView, attribute: .leading, relatedBy: .equal, toItem: contentView, attribute: .leading, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: headerView, attribute: .trailing, relatedBy: .equal, toItem: contentView, attribute: .trailing, multiplier: 1, constant: 0)
        ])
        
        headerView.addSubview(labelsStackView)
        labelsStackView.translatesAutoresizingMaskIntoConstraints = false
        labelsStackView.addArrangedSubviews([dateLabel, descriptionLabel])
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: labelsStackView, attribute: .top, relatedBy: .equal, toItem: headerView, attribute: .top, multiplier: 1, constant: 10),
            NSLayoutConstraint(item: labelsStackView, attribute: .leading, relatedBy: .equal, toItem: headerView, attribute: .leading, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: labelsStackView, attribute: .trailing, relatedBy: .equal, toItem: headerView, attribute: .trailing, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: labelsStackView, attribute: .bottom, relatedBy: .equal, toItem: headerView, attribute: .bottom, multiplier: 1, constant: -10)
        ])
        
        contentView.addSubview(statusLabel)
        statusLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: statusLabel, attribute: .top, relatedBy: .equal, toItem: headerView, attribute: .bottom, multiplier: 1, constant: 20),
            NSLayoutConstraint(item: statusLabel, attribute: .centerX, relatedBy: .equal, toItem: contentView, attribute: .centerX, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: statusLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 24),
            NSLayoutConstraint(item: statusLabel, attribute: .bottom, relatedBy: .equal, toItem: contentView, attribute: .bottom, multiplier: 1, constant: -20)
        ])
        
        let gestureRecognizer = UITapGestureRecognizer()
        dimmView.addGestureRecognizer(gestureRecognizer)
        gestureRecognizer.addTarget(self, action: #selector(moveOut))
        dimmView.frame = frame
        insertSubview(dimmView, belowSubview: contentView)
        
        moveIn()
    }
    
    func moveIn() {
        transform = CGAffineTransform(scaleX: 1.35, y: 1.35)
        alpha = 0.0
        UIView.animate(withDuration: 0.24) {
            self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.alpha = 1.0
        }
    }
    
    @objc func moveOut() {
        UIView.animate(withDuration: 0.24, animations: {
            self.transform = CGAffineTransform(scaleX: 1.35, y: 1.35)
            self.alpha = 0.0
        }) { _ in
            self.removeFromSuperview()
        }
    }
}

