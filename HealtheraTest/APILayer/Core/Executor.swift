//
//  Executor.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/19/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

protocol ExecutorProtocol {
    func execute(request: URLRequest,
                 errorHandler: ErrorHandlerProtocol,
                 _ completion: @escaping  ExecuteCompletion<Data>)
}

struct Executor: ExecutorProtocol {
    private var session: URLSession!
    
    init() {
        session = URLSession(configuration: .default)
    }
    
    func execute(request: URLRequest,
                 errorHandler: ErrorHandlerProtocol,
                 _ completion: @escaping  ExecuteCompletion<Data>) {
        let task = session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                completion(.reject(errorHandler.handle(error)))
            }
            
            if let response = response as? HTTPURLResponse,
               response.statusCode != 200 {
                completion(.reject(errorHandler.handle(response)))
            }
            
            if let data = data {
                completion(.success(data))
            }
        }
        task.resume()
    }
}

