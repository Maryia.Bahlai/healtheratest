//
//  Healthera.swift
//  HealtheraTest
//
//  Created by Баглай Мария on 10/19/20.
//  Copyright © 2020 Баглай Мария. All rights reserved.
//

import Foundation

enum HealtheraEndPoint: HTTPEndPoint {
    case login(AuthModel)
    case logout(AppUser)
    case adherences(AppUser)
    case remedy(RemedyModel)
    
    var baseURL: URL {
        return URL(string: "https://api.84r.co")!
    }
    
    var path: String {
        switch self {
        case .login, .logout:
            return "/tokens"
        case let .adherences(user):
            var path = "/patients/\(user.patientId)"
            if let start = user.start {
                path += "/adherences?start=\(start)"
            }
            if let end = user.end {
                path += "&end=\(end)"
            }
            return path
        case let .remedy(remedy):
            return "/patients/\(remedy.user.patientId)/remedies/\(remedy.remedyId)"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .login:
            return .post
        case .logout:
            return .delete
        case .adherences, .remedy:
            return .get
        }
    }
    
    var task: HTTPTask {
        switch self {
        case let .login(authModel):
            var bodyParameters: Parameters = ["username": authModel.username,
                                              "user_password": authModel.password]
            if let deviceToken = authModel.deviceToken {
                bodyParameters["device_token"] = deviceToken
            }
            return .requestAdditionHeadersAndBody(bodyParameters: bodyParameters,
                                                additionHeaders: headers,
                                                bodyEncoding: .jsonEncoding)
        case .logout:
            return .requestAdditionHeaders(additionHeaders: headers)
        case .adherences:
            return .requestAdditionHeaders(additionHeaders: headers)
        case .remedy:
            return .requestAdditionHeaders(additionHeaders: headers)
        }
    }
    
    var headers: HTTPHeaders? {
        switch self {
        case let .login(authModel):
            return ["client-id": authModel.clientId]
        case let .logout(user):
            return setupCommonHeaders(with: user)
        case let .adherences(user):
            return setupCommonHeaders(with: user)
        case let .remedy(remedy):
            return setupCommonHeaders(with: remedy.user)
        }
    }
    
    private func setupCommonHeaders(with user: AppUser) -> HTTPHeaders? {
        return ["Token": user.jwtToken,
                "client-id": user.clientId,
                "app-platform": DeviceInfo.getDevicePlatform(),
                "app-version": user.appVersion]
    }
}
